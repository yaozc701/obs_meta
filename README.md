# obs_meta

### 介绍
1.该项目用于管理obs系统多个仓库的软件包，通过_service文件达到对obs仓库中软件包的增、删、改、查目的。        
2.该项目用于管理obs工程的管理，通过对相关工程的对应的meta文件的修改实现对obs工程的控制管理。

### 软件目录结构
```xml
obs_meta（码云仓库名称） 
       
| -- 软件包码云仓库分支名称        
    | -- obs系统中软件包在对应分支下的工程        
        | -- 软件包名称        
            | -- 软件包对应的_service文件     
| -- multi_version
        | -- multi相关码云仓库分支名称
            | -- multi分支中相关obs工程名
                | -- 软件包名称
                    | -- 软件包对应的_service文件
| -- OBS_PRJ_meta        
    | -- 码云仓库分支名称        
        | -- obs系统中对应分支下的工程meta文件(工程名命名)         
        | -- obs系统中对应分支下的工程的project_config备份文件（prjconf + obs工程名）
    | -- multi_version        
        | -- multi相关码云仓库分支名称        
            | -- multi相关obs工程meta文件(工程名命名)
            | -- multi相关obs工程的project_config备份文件（prjconf + obs工程名）
```
              
##### 示例：  
```d 
obs_meta

Example1:
    | -- openEuler-20.03-LTS-SP1                    
        | -- openEuler:20.03:LTS:SP1    
            | -- gcc
                | -- _service
            | -- mysql5
                | -- _service
        | -- openEuler:20.03:LTS:SP1:Epol
            | -- artil
                | -- _service

Example2:
    | -- multi_version
        | -- Multi-Version_OpenStack-Rocky_openEuler-20.03-LTS-SP2
            | -- openEuler:20.03:LTS:SP2:Epol:Multi-Version:OpenStack:Rocky
                | -- openstack-nova
                    | -- _service
                | -- openstack-horizion
                    | -- _service

Example3:
    | -- OBS_PRJ_meta
        | -- master
             | -- openEuler:Mainline [metafile]
             | -- openEuler:Factory [metafile]
        | -- openEuler-20.03-LTS-SP1
             | -- openEuler:20.03:LTS:SP1 [metafile]
             | -- openEuler:20.03:LTS:SP1:Epol [metafile]
        | -- multi_version
            | -- Multi-Version_OpenStack-Rocky_openEuler-20.03-LTS-SP2
                | -- openEuler:20.03:LTS:SP2:Epol:Multi-Version:OpenStack:Rocky [metafile]
            | -- Multi-Version_OpenStack-Queens_openEuler-20.03-LTS-SP2
                | -- openEuler:20.03:LTS:SP2:Epol:Multi-Version:OpenStack:Queens [metafile]
    
```

### 使用说明
##### 新增软件包

在fork出来的个人仓库 obs_meta 仓库的对应目录（master分支下openeuler:Factory）创建 "<软件包名>/_service" 文件内容参照实例如下：
```xml
<services>
    <service name="tar_scm_kernel_repo">
      <param name="scm">repo</param>
      <param name="url">next/分支名（master分支用openeuler表示）/软件包名</param>
    </service>
</services>
```

##### 移动软件包

> **常用于将obs系统里同分支的 openEuler:Factory 仓库编译成功的包，移动到 openEuler:Mainline 仓库**

	1.在个人码云仓库obs_meta仓库下，将相应分支及工程下软件包移动到相同分支下另一工程的目录中.
        如：master/openEuler:Factory/gcc -->master/openEuler:Mainline/gcc
	2.通过个人码云仓库 obs_meta 向 obs_meta 主仓库提交pr，等待pr被接受
	3.pr被接受后，后台会删除obs系统原仓库的包即openEuler:Factory仓库里对应的软件包，在新的仓库如openEuler:Mainline中创建对应的软件包

##### OBS新增工程

用于在obs系统中创建公有工程，在个人码云仓库obs_meta仓库下OBS_PRJ_meta中对应分支的目录下创建以工程名命名的meta文件后提交Pull request。
如：OBS_PRJ_meta/master/openEuler:Mainline
```xml
<project name="openEuler:Mainline">
  <title/>
  <description/>
  <person userid="Admin" role="maintainer"/>
  <repository name="standard_x86_64">
    <path project="openEuler:selfbuild:BaseOS" repository="mainline_standard_x86_64"/>
    <arch>x86_64</arch>
  </repository>
  <repository name="standard_aarch64">
    <path project="openEuler:selfbuild:BaseOS" repository="mainline_standard_aarch64"/>
    <arch>aarch64</arch>
  </repository>
</project>
```

### 特别注意：
&emsp;&emsp;● 一个Pull Request请对应一个提交        
&emsp;&emsp;● 相同分支下请确保软件包只存在于一个工程目录中  
&emsp;&emsp;● obs_meta/OBS_PRJ_meta中对应分支目录里相关prjconf为首命名的文件为相应工程的project_config文件备份        
&emsp;&emsp;● multi_version多版本分支中，目录层级与普通有所差异，采用将所有multi分支统一存放到multi_version目录中，其他层级不变，如上述Example2与Example3        
&emsp;&emsp;● 新增的软件包会先加到obs_meta/master/openeuler:Factory目录，加入之后会在obs系统的openeuler:Factory仓库创建相应的软件包        
&emsp;&emsp;● 请保证软件包_service文件的url中分支与软件包名与当前service存在路径的分支包名对应(master分支对应openeuler)，且软件包所在分支为受保护分支，确保文件格式正确        
&emsp;&emsp;● OBS_PRJ_meta中meta文件请保证至少存在一位user，对应的project以及repository已存在于obs系统工程中，确保文件格式正确

   

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



